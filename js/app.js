$(document).foundation()

$(window).ready(function () {
	coverSlider = $('#cover_carousel_container').bxSlider({
		controls: false,
		mode: "fade"
	});
	
	//scroll check
	var wHeight = $(window).innerHeight();
	var siblings = $('section');
	var menuItems = $('#sections_menu ul li a');
	var perset = {};
	var sumHeight = 0;
	for(var i = 0; i<siblings.length; i++) {
	  	perset[sumHeight] = i;
	    sumHeight= sumHeight + siblings[i].clientHeight;
	}
	processScroll();
	  
	
	  function lessThan(nums, key){
	    if(nums == null || nums.length == 0 || key ==0 ) 
	      return 0;
	    low = 0;
	    high = nums.length -1;
	    while(low <= high){
	        mid = parseInt((low + high) >> 1);
	        if(key <= nums[mid]){
	            high = mid - 1;
	        }else {
	            low = mid +1;
	        }
	    }
	    return high;
	  }
	
	  var scroll_pos = 0;
	  var cur_slide = 0;
	  
	  function processScroll() { 
	  	scroll_pos = $(this).scrollTop();
		
	    var presetHeights = Object.keys(perset);
	    var x = lessThan(presetHeights,scroll_pos);
	    var goto = perset[presetHeights[x]];
	    
	    if (goto != cur_slide && goto != undefined) {
	    	coverSlider.goToSlide(goto);
	    	cur_slide = goto;
	    	activeNav(cur_slide)
	    }
	    
	  }
	  
	  
	  function activeNav(cur_slide) {
	  	menuItems.removeClass('active');
	  	$(menuItems[cur_slide]).addClass('active');
	   
	  }
	
	  $(document).scroll(processScroll);
	  
})


$(document).ready(function(){
    
    var getMax = function(){
        return $(document).height() - $(window).height();
    }
    
    var getValue = function(){
        return $(window).scrollTop();
    }
    
    if('max' in document.createElement('progress')){
        // Browser supports progress element
        var progressBar = $('progress');
        
        // Set the Max attr for the first time
        progressBar.attr({ max: getMax() });

        $(document).on('scroll', function(){
            // On scroll only Value attr needs to be calculated
            progressBar.attr({ value: getValue() });
        });
      
        $(window).resize(function(){
            // On resize, both Max/Value attr needs to be calculated
            progressBar.attr({ max: getMax(), value: getValue() });
        });   
    }
    else {
        var progressBar = $('.progress-bar'), 
            max = getMax(), 
            value, width;
        
        var getWidth = function(){
            // Calculate width in percentage
            value = getValue();            
            width = (value/max) * 100;
            width = width + '%';
            return width;
        }
        
        var setWidth = function(){
            progressBar.css({ width: getWidth() });
        }
        
        $(document).on('scroll', setWidth);
        $(window).on('resize', function(){
            // Need to reset the Max attr
            max = getMax();
            setWidth();
        });
    }
});



/*PROGRESS*/
$(document).ready(function(){
  
  $('#flat').addClass("active");
  $('#progressBar').addClass('flat');
    
  $('#flat').on('click', function(){
    $('#progressBar').removeClass().addClass('flat');
    $('a').removeClass();
    $(this).addClass('active');
    $(this).preventDefault();
  });

  $('#single').on('click', function(){
    $('#progressBar').removeClass().addClass('single');
    $('a').removeClass();    
    $(this).addClass('active');
    $(this).preventDefault();    
  });

  $('#multiple').on('click', function(){
    $('#progressBar').removeClass().addClass('multiple');
    $('a').removeClass();    
    $(this).addClass('active');
    $(this).preventDefault();    
  });

  $('#semantic').on('click', function(){
    $('#progressBar').removeClass().addClass('semantic');
    $('a').removeClass();    
    $(this).addClass('active');
    $(this).preventDefault();
    alert('hello');
  });

  $(document).on('scroll', function(){

      maxAttr = $('#progressBar').attr('max');
      valueAttr = $('#progressBar').attr('value');
      percentage = (valueAttr/maxAttr) * 100;
      
  });
  
});
